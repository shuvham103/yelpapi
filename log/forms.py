from django import forms


class BusinessForm(forms.Form):
   business = forms.CharField(max_length = 50,required=True)
   location = forms.CharField(max_length=20,required=True)
   page=forms.IntegerField(min_value=1, required=True)
   limit=page=forms.IntegerField(min_value=1,max_value=50,required=True)