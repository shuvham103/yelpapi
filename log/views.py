from django.http import HttpResponse, JsonResponse,HttpResponseRedirect
from pymongo import mongo_client,MongoClient
import sys, os
import json
from django.contrib.auth import authenticate
from django.shortcuts import render, redirect
import requests
from log.forms import BusinessForm
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
import pandas as pd
from pandas.io.json import json_normalize
from django.views.static import serve

# To connect to mongo db we use MonoClient
def mongo_conn():
    try:
        print("mongo")
        #make sure the host and port are correct. Usually is the settings is unmodified 
        conn = MongoClient(host='127.0.0.1', port=27017)
        # print("MongoDB Connected", conn)
        return conn
    except Exception as e:
        print("Error in mongo connection: ", e)

#to make sure the connection is established every single time it is declared globally
dbconn=mongo_conn().business


def yelpapi(request):
    #if it is a get method then we need to check if the user is authenticated
    if (request.method=="GET"):
        #checking for the authentication and redirecting to login if not authorized
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse("login"))

        #Form in log.model for business
        form=BusinessForm()
        return render(request,"form.html",{"form":form})

    #if the form is submitted
    if (request.method =="POST"):
        
        #the datat provided by the POST form or the business form is checked if it is valid
        data=BusinessForm(request.POST)
        if data.is_valid():
            ##For yelp we need params and body
            ##params consists of terms:what type of business, location: can be zip code city, limit: max value 50 and offset
            params={'term':data.cleaned_data["business"],'location':data.cleaned_data["location"], 'limit':data.cleaned_data["limit"],"offset":(data.cleaned_data["page"]-1)*data.cleaned_data["limit"]}
            #header should consists of bearer token that is provided by yelp
            headers = {"Authorization": bearer()}

            #declaring an empty dictionary object
            business_to_return={}

            #empty the csv file so that new data can be added
            f = open("output.csv", "w")
            f.truncate()
            f.close()
            
            #requesting to yelp url, with categories of params and headers
            business_to_return=requests.get(url=URL()+"search",params=params, headers=headers)

            #the response has everything under reponse.text which is stored
            business_to_return=json.loads(business_to_return.text)
            # Looping through all the business
            for business in (business_to_return.get("businesses")):

                #the _id is used for the mongo db id so the data wont be repeated
                business['_id']=business.get('id')

                #this time we want to get more detail
                second_call_for_detail=requests.get(url=URL()+business.get('id'), params={"id":id}, headers=headers)

                #again the request is 
                jsonify=json.loads(second_call_for_detail.text)

                #save it to mongo db
                save_in_mongo(jsonify,params,business)
            
            businessforjson=business_to_return.get("businesses")
            df=pd.DataFrame(businessforjson)
            df.to_json('log/templates/output.json')

    business_to_return=business_to_return.get("businesses")
     #for now I returned sucessfully updated. I will return a text box that you can select and convert to csv
    return render(request, "response.html", {
                "data_to_return": business_to_return
            })    
def bearer():
    return "Bearer 1VsOeaV7KKT9zTXad0RLmDDzoFdTgMb_aQcwYook7cTlkuztt5GSqidnDLHg26xzZiLRDKOXdIX4bETXJ4xhDl9yJ4ggVtmr_WuhhABN04sAsFSrIHhLGKaO5ZaJX3Yx"
    
def URL():
    return "https://api.yelp.com/v3/businesses/"


#converts and save the essential field to mongo. Can be added or removed as per requirements
def save_in_mongo(jsonified,params,business={}):

    data_to_save_in_db={}
    data_to_save_in_db["_id"]=jsonified["id"]
    data_to_save_in_db["search_term"]=params
    data_to_save_in_db["category"]=jsonified["categories"]
    data_to_save_in_db["name"]=jsonified["name"]
    data_to_save_in_db["business_address_1"]=jsonified["location"].get("address1")
    data_to_save_in_db["business_address_2"]=jsonified["location"].get("address2")
    data_to_save_in_db["city"]=jsonified["location"].get("city")
    data_to_save_in_db["zip"]=jsonified["location"].get("zip_code")
    data_to_save_in_db["business_phone"]=jsonified["phone"]
    data_to_save_in_db["state"]=jsonified["location"].get("state")
    data_to_save_in_db["url"]=jsonified["url"]
    data_to_save_in_db["email"]=""
    data_to_save_in_db["image_url"]=jsonified["image_url"]
    data_to_save_in_db["Note"]=jsonified["location"].get("cross_streets")
    data_to_save_in_db["yelp_page"]=jsonified["url"]
    # A business might not have updated hours so it is added to prevent any errors
    try:
        data_to_save_in_db["Business_Operation_Day"]=len(jsonified["hours"][0]["open"])
        business["Business_Operation_Day"]=len(jsonified["hours"][0]["open"])
        data_to_save_in_db["operation_hours"]=jsonified["hours"][0]["open"]
        business["operation_hours"]=jsonified["hours"][0]["open"]
        dbconn.business.save(data_to_save_in_db)
        
    except:
        dbconn.business.save(data_to_save_in_db)


    





        

    
#For the UI purpose, thr first time user needs to login or be authenticated

def login_view(request):
    #if request is POST or the form is filled
    if request.method == "POST":

        username = request.POST["username"]
        password = request.POST["password"]

        #authenticating the user
        user = authenticate(request, username=username, password=password)

        #if the user is valid
        if user is not None:

            #Then login and return the form for Yelp API, other functionality can be added in the form
            login(request, user)
            return HttpResponseRedirect(reverse("yelpapi"))
        else:
            #else send the login form with error Invalid Credentials
            return render(request, "login.html", {
                "message": "Invalid credentials."
            })
    #For anything except GET user needs to fill the login form
    else:
        return render(request, "login.html")

# Just a function for log out

def logout_view(request):
    #Uses django login to log out user completely
    logout(request)
    #return the login form with message logout
    return render(request, "login.html", {
        "message": "Logged out."
    })

def change_password(reauest):
    return redirect( "http://127.0.0.1:8000/admin/password_change/")


def output(request):
    filepath="log/templates/output.json"
    return serve(request, os.path.basename(filepath),os.path.dirname(filepath))


