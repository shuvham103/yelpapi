from django.urls import path, include
from . import views

urlpatterns = [

    path('',  views.yelpapi, name='yelpapi',),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("change_pw",views.change_password,name="changepw"),
    path("output",views.output,name="output")

]